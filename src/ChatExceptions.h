#ifndef NOVOSOFT_CHATEXCEPTIONS_H
#define NOVOSOFT_CHATEXCEPTIONS_H

#include <exception>
#include <string>

class IncorrectChatFile: public std::exception {
public:
    [[nodiscard]] const char *what() const noexcept override {
        return "Chat file is incorrect";
    };
};

class IncorrectClientCount: public std::exception {
public:
    [[nodiscard]] const char *what() const noexcept override {
        return "Number of chat clients must be a positive number less than 2147483648";
    };
};

class IncorrectLatency: public std::exception {
public:
    [[nodiscard]] const char *what() const noexcept override {
        return "Latency must be a positive number less than 2147483648";
    };
};

#endif //NOVOSOFT_CHATEXCEPTIONS_H
