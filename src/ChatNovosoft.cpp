#include "ChatExceptions.h"
#include "ChatNovosoft.h"
#include <fstream>
#include <chrono>
#include <iostream>
#include <mutex>
#include <thread>

bool operator< (Client const& left, Client const& right) {
    return left.getId() < right.getId();
}

/**
 * This function stop program
 */
void Chat::stopFunction() {
    std::cout << "Press enter to stop the program" << std::endl;
    int c = getchar();
    std::lock_guard<std::mutex> lk(cv_mutex);
    cv.notify_one();
}

Chat::Chat(std::istream& chatInfoStream) {
    chatClosed = false;
    try {
        initChat(chatInfoStream);
    } catch (IncorrectLatency& exception) {
        throw exception;
    } catch (IncorrectClientCount& exception) {
        throw exception;
    } catch (IncorrectChatFile& exception) {
        throw exception;
    }
}

void Chat::initChat(std::istream& chatInfoInput) {
    int countClients;
    chatInfoInput >> countClients;
    if (countClients <= 0 || chatInfoInput.fail()) {
        throw IncorrectClientCount();
    }
    auto start = std::chrono::system_clock::now();
    timeoutsMessages = std::map<std::chrono::time_point<std::chrono::system_clock>, std::set<Client>>();
    for (int i = 0; i < countClients; ++i) {
        if (chatInfoInput.eof()) {
            throw IncorrectChatFile();
        }
        int latency;
        chatInfoInput >> latency;
        if (latency <= 0 || chatInfoInput.fail()) {
            throw IncorrectLatency();
        }
        auto messageTime = start + std::chrono::seconds(latency);
        std::string message;
        std::getline(chatInfoInput, message);
        addClient(messageTime, Client(i, message, latency));
    }
}

/**
 * Function with main cycle
 * Program gets a message with a minimum time at each step
 * After writing a message, an old time will be deleted and a new time will be added
 */
void Chat::startChat(const std::string& outputFileName) {
    std::ofstream outputChat = std::ofstream(outputFileName);
    std::thread stopThread(&Chat::stopFunction, this);
    while (!chatClosed) {
        auto minTimeoutsClients = timeoutsMessages.begin();
        std::unique_lock<std::mutex> lk(cv_mutex);
        if (cv.wait_until(lk, minTimeoutsClients->first) == std::cv_status::timeout) {
            for (const Client& client : minTimeoutsClients->second) {
                outputChat << getTime() << "; " << client.getId() << ": " << client.getMessage() << std::endl;
                addClient(minTimeoutsClients->first + client.getLatency(), client);
            }
        } else {
            chatClosed = true;
            break;
        }
        timeoutsMessages.erase(minTimeoutsClients->first);
    }
    outputChat.close();
    stopThread.join();
}

void Chat::addClient(std::chrono::time_point<std::chrono::system_clock> timeMessage, const Client& client) {
    if (timeoutsMessages.find(timeMessage) != timeoutsMessages.end()) {
        timeoutsMessages[timeMessage].insert(client);
    }
    else {
        timeoutsMessages.insert(std::pair<std::chrono::time_point<std::chrono::system_clock>, std::set<Client> >
                                (timeMessage, {client}));
    }
}

// https://stackoverflow.com/questions/997946/how-to-get-current-time-and-date-in-c
std::string Chat::getTime() {
    std::time_t timeNow = std::time(0);
    std::string time = std::ctime(&timeNow);
    return time.substr(11, 8);
}

int Client::getId() const {
    return id;
}

std::string Client::getMessage() const {
    return message;
}

std::chrono::seconds Client::getLatency() const {
    return latency;
}
