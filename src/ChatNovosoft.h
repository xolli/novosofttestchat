#ifndef NOVOSOFT_CHATNOVOSOFT_H
#define NOVOSOFT_CHATNOVOSOFT_H

#include "ChatExceptions.h"
#include <string>
#include <map>
#include <set>
#include <condition_variable>

class Client {
    std::string message;
    int id;
    std::chrono::seconds latency;
public:
    Client(int id, std::string& message, int latency) : id(id), message(message), latency(latency) {}
    std::string getMessage() const;
    int getId() const;
    std::chrono::seconds getLatency() const;
};

class Chat {
    std::map<std::chrono::time_point<std::chrono::system_clock>, std::set<Client>> timeoutsMessages; // message time -> message
    std::condition_variable cv;
    std::mutex cv_mutex;
    bool chatClosed;
public:
    explicit Chat(std::istream& chatInfoStream);
    void startChat(const std::string& outputFileName);
private:
    void initChat(std::istream&);
    static std::string getTime();
    void stopFunction();
    void addClient(std::chrono::time_point<std::chrono::system_clock> timeMessage, const Client& client);
};

#endif //NOVOSOFT_CHATNOVOSOFT_H
