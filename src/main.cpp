#include <iostream>
#include "ChatNovosoft.h"

int main() {
    std::string outputFileName;
    std::cin >> outputFileName;
    try {
        Chat chat = Chat(std::cin);
        chat.startChat(std::string(outputFileName));
    } catch (IncorrectLatency& exception) {
        std::cerr << exception.what() << std::endl;
    } catch (IncorrectClientCount& exception) {
        std::cerr << exception.what() << std::endl;
    } catch (IncorrectChatFile& exception) {
        std::cerr << exception.what() << std::endl;
    }
    return 0;
}

/**
 * @author Игорь Казак
 * @date 18.04.2021
 *
 * Test chat
 */
